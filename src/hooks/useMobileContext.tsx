import { useContext } from 'react'
import { MobileContext } from '../context/mobileContext'


const useMobileContext = () => {
const mobileContext = useContext(MobileContext)

  return mobileContext
}

export default useMobileContext