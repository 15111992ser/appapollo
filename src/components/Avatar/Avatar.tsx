import React from "react";
import classNames from "classnames";
import styles from "./Avatar.module.scss";

interface IPropsAvatar {
  isLarge: boolean;
  avatarUrl?: string | undefined;
}

const Avatar: React.FC<IPropsAvatar> = ({ isLarge, avatarUrl }) => {
  return (
    <div className={classNames(styles.avatar, { [styles.large]: isLarge })}>
      {avatarUrl ? <img src={avatarUrl} alt="av" /> : "AV"}
    </div>
  );
};

export default Avatar;
