import { useState } from "react";
import styles from "./BurgerMenu.module.scss";

interface IBurgerMenuProps{
    isVisibleList: boolean;
    setIsVisibleList: (payload: boolean) => void
  }

const BurgerMenu: React.FC<IBurgerMenuProps> = ({isVisibleList, setIsVisibleList}) => {

  return (
    <div
      className={styles.burgerBtn}
      onClick={() => {
        setIsVisibleList(!isVisibleList);
      }}
    >
      <span></span>
    </div>
  );
};

export default BurgerMenu;
