import React from 'react'
import styles from './Error.module.scss'
import { useForm, FieldError } from "react-hook-form";

interface IErrorComponentPropps{
  err?: string 
}

const ErrorComponent : React.FC<IErrorComponentPropps> = (props) => {
  return (
    <div className={styles.errorBlock}>{props?.err}</div>
  )
}

export default ErrorComponent
