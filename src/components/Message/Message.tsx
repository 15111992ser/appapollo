import Avatar from "../Avatar/Avatar";
import styles from "./Message.module.scss";
import cn from "classnames";
import { getFormatDateMessages } from "../../utils/formatTime";


interface IMessageProps {
  isIncomingMessages?: boolean;
  messageText: string;
  avatar: string | undefined;
  date: string
}

const Message: React.FC<IMessageProps> = ({ isIncomingMessages, messageText, avatar, date }) => {
  const dateForMessages = getFormatDateMessages(date)

  return (
    <div data-testid="wrapper" className={cn(styles.wrapper, isIncomingMessages && styles.left)}>
      <div data-testid="container" className={cn(styles.container, isIncomingMessages && styles.left)}>
        <div>
          <p className={cn(styles.messageBody, isIncomingMessages && styles.left)}>
            {messageText}
          </p>
          <div className={cn(styles.messageDate, isIncomingMessages && styles.left)}>{dateForMessages}</div>
        </div>
        <div className={cn(styles.messageAvatar, isIncomingMessages && styles.left)}>
          <Avatar isLarge={false} avatarUrl={avatar} />
        </div>
      </div>
    </div>
  );
};

export default Message;
