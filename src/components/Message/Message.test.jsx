import { render, screen } from "@testing-library/react";
import styles from "./Message.module.scss";
import Message from "./Message";

const messageData = {
  messageText:
    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque, velit! Ab, quos omnis! Neque quae provident tempore nam qui laudantium optio sequi nostrum fugit? Veniam et voluptatibus saepe perspiciatis autem.",
  avatar:
    "https://cdn5.vectorstock.com/i/thumb-large/99/94/default-avatar-placeholder-profile-icon-male-vector-23889994.jpg",
  date: "02.24.2022",
};

describe("Message component", () => {
  it("Message componet snapshot", () => {
    const view = render(
      <Message
        avatar={messageData.avatar}
        messageText={messageData.messageText}
        date={messageData.date}
      />
    );
    expect(view).toMatchSnapshot();
  });

  it("renders Message component", () => {
    render(
      <Message
        avatar={messageData.avatar}
        messageText={messageData.messageText}
        date={messageData.date}
      />
    );
    expect(screen.getByText(messageData.messageText)).toBeInTheDocument();
  });

  it("dynamic styles works (props - isIncomingMessages={true})", () => {
    render(
      <Message
        avatar={messageData.avatar}
        messageText={messageData.messageText}
        date={messageData.date}
        isIncomingMessages
      />
    );

    expect(screen.getByText(messageData.messageText)).toHaveClass(styles.left);
    expect(screen.getByText(messageData.date)).toHaveClass(styles.left);
    expect(screen.getByTestId("wrapper")).toHaveClass(styles.left);
    expect(screen.getByTestId("container")).toHaveClass(styles.left);
  });
});
