import React from "react";
import classNames from "classnames";
import styles from "./Input.module.scss";

interface IPropsInput {
  register?: any;
  type: string;
  error?: string;
  placeholder: string;
  title?: string;
}

const Input: React.FC<IPropsInput> = ({ register, title, error, type, ...props }) => {
  
  return (
    <div className={styles.wrapper}>
      {title && <div className={styles.title}>{title}</div>}
      <input
        {...register}
        type={type}
        {...props}
        className={classNames(styles.input, { [styles.error]: error })}
      />
      {error && <div className={styles.errorText}>{error}</div>}
    </div>
  );
};

export default Input;
