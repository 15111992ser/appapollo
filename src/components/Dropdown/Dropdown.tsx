import React, { ReactNode, useRef, useState } from "react";
import Portal from "../Portal/Portal";

interface IDropdownProps {
  target: ReactNode;
  classNames?: string;
  children?: ReactNode | string;
}

const Dropdown: React.FC<IDropdownProps> = ({ classNames, children, target }) => {
  const [isOpenDropdown, setIsOpenDropdown] = useState(false);
  const targetNode = useRef<HTMLHeadingElement>(null);
  const rect: DOMRect | undefined = targetNode?.current?.getBoundingClientRect();

  const toggleOpenDropdown = () => {
    setIsOpenDropdown(!isOpenDropdown);
  };

  const closeDropdown = () => {
    setIsOpenDropdown(false);
  };

  return (
    <div>
      <div className="target" ref={targetNode} onClick={toggleOpenDropdown}>
        {target}
      </div>
      <div className={classNames}>
        {isOpenDropdown && (
          <Portal closeDropdown={closeDropdown} rect={rect}>
            {children}
          </Portal>
        )}
      </div>
    </div>
  );
};

export default Dropdown;
