import React from "react";
import { Link, useLocation } from "react-router-dom";
import useAuthContext from "../../hooks/useAuthContext";
import useMobileContext from "../../hooks/useMobileContext";
import Avatar from "../Avatar/Avatar";
import BurgerMenu from "../BurgerMenu/BurgerMenu";
import Dropdown from "../Dropdown/Dropdown";
import styles from "./Header.module.scss";



const Header: React.FC = () => {
  const {isVisibleList, setIsVisibleList} = useMobileContext()
  const { user, isAuth, logout } = useAuthContext();
  const { pathname } = useLocation();
  const isChatPage = pathname === "/";

  return (
    <div className={styles.header}>
      <div className={styles.headerInner}>
        <div className={styles.logo}>
          {isAuth && isChatPage && (
            <BurgerMenu isVisibleList={isVisibleList} setIsVisibleList={setIsVisibleList} />
          )}
          <Link to="/"> GQL Chat</Link>
        </div>
        {isAuth ? (
          <div>
            <Dropdown target={<Avatar avatarUrl={user?.avatar} isLarge={false} />}>
              <ul className={styles.menuList}>
                <li>
                  <Link to="/profile">Profile</Link>
                </li>
                <li onClick={logout}>Logout</li>
              </ul>
            </Dropdown>
          </div>
        ) : (
          <div className={styles.login}>
            <Link to="/login">Login</Link>
          </div>
        )}
      </div>
    </div>
  );
};

export default Header;
