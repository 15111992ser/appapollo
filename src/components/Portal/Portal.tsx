import { ReactElement, ReactNode } from "react";
import ReactDOM from "react-dom";

export interface IModalProps {
  children?: ReactNode | ReactElement;
  closeDropdown: VoidFunction;
  rect: DOMRect | undefined;
}

const root = document.getElementById("modal-root");

const Portal: React.FC<IModalProps> = ({ children, closeDropdown, rect }) => {
  return root
    ? ReactDOM.createPortal(
        <div className="portalContainer" onClick={closeDropdown}>
          <div
            onClick={(e) => e.stopPropagation()}
            className="dropdown"
            style={{ left: rect?.right, top: rect?.bottom }}
          >
            <div onClick={closeDropdown}>{children}</div>
          </div>
        </div>,
        root
      )
    : null;
};
export default Portal;
