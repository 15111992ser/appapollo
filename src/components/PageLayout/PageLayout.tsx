import React from "react";
import styles from "./PageLayout.module.scss";

interface IPropsChatLayout {
  children: JSX.Element[] | JSX.Element;
}

const PageLayout: React.FC<IPropsChatLayout> = (props) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.layout}>{props.children}</div>
    </div>
  );
};

export default PageLayout;
