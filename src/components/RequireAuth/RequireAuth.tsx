import { Navigate, useLocation} from "react-router-dom";
import useAuthContext from "../../hooks/useAuthContext";

interface IPropsRequireAuth {
  children: JSX.Element[] | JSX.Element;
}

const RequireAuth: React.FC<IPropsRequireAuth> = (props) => {

    const location = useLocation()
    const {isAuth} = useAuthContext()

    if(!isAuth && !localStorage.getItem("access-token")) {
      return <Navigate to="../login"  state={{from: location}} replace/>
    }

  return <>{props.children}</>;
};

export default RequireAuth;
