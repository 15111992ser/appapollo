import cn from "classnames";
import { useState } from "react";
import Button from "../Button/Button";
import Input from "../Input/Input";
import styles from "./AddRoom.module.scss";
import { useForm, SubmitHandler } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { CREATE_CONVERSATION } from "../../common/apolloClient/gql/mutation/createConversation";
import { useMutation } from "@apollo/client";
import { IConversation } from "../../models/conversations";

interface ICreateConv{
  createConversation: IConversation
}

interface IUseFormNameConv {
  name: string;
}

const schema = yup.object().shape({
  name: yup.string().trim().required(),
});

const AddRoom: React.FC = () => {
  const [isOpen, setIsOpen] = useState(false);
  const { register, handleSubmit, reset} = useForm<IUseFormNameConv>({
    resolver: yupResolver(schema),
  });

  const [createConv] = useMutation<ICreateConv>(CREATE_CONVERSATION);

  const submitForm: SubmitHandler<IUseFormNameConv> = async (data) => {
    const res = await createConv({
      variables: { name: data.name },
    });
    reset()
    setIsOpen(false)
  };

  return (
    <div className={styles.wrapper}>
      <div className={cn(styles.addRoomName,isOpen &&  styles.open)}>
        <span>Rooms</span>
        <div
          onClick={() => { setIsOpen(!isOpen)}}
          className={styles.addRoomBtn}
        >
          <div className={styles.item}></div>
          <div className={cn(styles.item, styles.vertical, isOpen && styles.open)}></div>
        </div>
      </div>
      {isOpen && (
        <form className={styles.addRoomForm} onSubmit={handleSubmit(submitForm)}>
          <Input placeholder="Name" type="text" title="Input"  register={register("name")}/>
          <div className={styles.wrapperBtn}>
            <Button type="submit" bgType="primary">Add</Button>
          </div>
        </form>
      )}
    </div>
  );
};

export default AddRoom;