import styles from "./PreLoader.module.scss";

interface IPreLoaderProps {
  text?: string;
}

const PreLoader: React.FC<IPreLoaderProps> = ({ text }) => {
  return (
    <div className={styles.loader}>
      <span>{text ? text : "Loading..."}</span>
    </div>
  );
};

export default PreLoader;
