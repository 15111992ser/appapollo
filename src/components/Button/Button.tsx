import React from "react";
import classNames from "classnames";
import styles from "./Button.module.scss";
interface IPropsButton {
  type?: "submit";
  children: string | JSX.Element;
  bgType: "primary" | "secondary";
  disabled?: boolean;
  cb?: VoidFunction;
}

const bgColor = { primary: styles.primary, secondary: styles.secondary };

const Button: React.FC<IPropsButton> = ({ bgType, children,cb,disabled, ...props }) => {
  const currentBgColor = bgColor[bgType] ?? bgColor.primary;

  return (
    <button onClick={cb} {...props} className={classNames(styles.btn, currentBgColor, disabled &&  styles.disabledBtn)}>
      {children}
    </button>
  );
};

export default Button;
