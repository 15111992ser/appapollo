import React from "react";
import styles from "./RegistrationPage.module.scss";
import PageLayout from "../../components/PageLayout/PageLayout";
import Input from "../../components/Input/Input";
import Avatar from "../../components/Avatar/Avatar";
import Button from "../../components/Button/Button";
import ErrorComponent from "../../components/Error/ErrorComponenet";
import { useMutation } from "@apollo/client";
import { useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import useAuthContext from "../../hooks/useAuthContext";
import { IRegistration } from "../../models/auth";
import { REGISTER_USER } from "../../common/apolloClient/gql/mutation/registerUser";

interface IUseForm {
  email: string;
  password: string;
  login: string;
  repeatPassword: string;
  avatarUrl: string;
}

const schema = yup.object().shape({
  login: yup.string().required("Login is required"),
  email: yup.string().email().required(),
  password: yup.string().min(6).max(32).required(),
  repeatPassword: yup.string().oneOf([yup.ref("password"), null], "Passwords must match"),
  avatarUrl: yup.string().url(),
});

const RegistrationPage: React.FC = () => {
  const { login } = useAuthContext();
  const navigate = useNavigate();
  const [addUser, { error, loading }] = useMutation<IRegistration>(REGISTER_USER);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IUseForm>({
    resolver: yupResolver(schema),
  });

  const submitForm = async (data: IUseForm) => {
    const res = await addUser({
      variables: {
        login: data.login,
        password: data.password,
        avatar: data.avatarUrl,
        email: data.email,
      },
    });

    if (res.data) {
      login(res.data?.registration, () => {
        navigate("/");
      });
    }
  };

  return (
    <PageLayout>
      <div className="wrapperForm">
        <h1 className="titleForm">Registration</h1>
        {error && <ErrorComponent err={error?.message} />}
        <div className="wrapperFormInner">
          <form className={styles.form} onSubmit={handleSubmit(submitForm)}>
            <Input
              register={register("login")}
              error={errors.login?.message}
              type="text"
              placeholder="Login"
              title="Login"
            />

            <Input
              register={register("email")}
              error={errors.email?.message}
              type="text"
              placeholder="Email"
              title="Email"
            />

            <Input
              register={register("password")}
              error={errors.password?.message}
              type="password"
              placeholder="Password"
              title="Password"
            />

            <Input
              register={register("repeatPassword")}
              error={errors.repeatPassword?.message}
              type="password"
              placeholder="Password"
              title="Repeat password"
            />

            <div className={styles.avatarBlockWrapper}>
              <div className={styles.avatarBlock}>
                <Avatar isLarge />
              </div>
              <div className={styles.urlBlock}>
                <Input
                  register={register("avatarUrl")}
                  error={errors.avatarUrl?.message}
                  type="text"
                  placeholder="Url"
                  title="Avatar URL"
                />
              </div>
            </div>

            <div className={styles.btnContainer}>
              <Button type="submit" bgType="primary" disabled={loading}>
                Registration
              </Button>
            </div>
          </form>
        </div>
      </div>
    </PageLayout>
  );
};

export default RegistrationPage;
