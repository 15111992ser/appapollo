import React from "react";
import { Chart as ChartJS, CategoryScale, LinearScale, BarElement, Tooltip } from "chart.js";
import { Bar } from "react-chartjs-2";
import { IStatistics } from "../../models/message";

const labels = [
  "6 days ago",
  "5 days ago",
  "4 days ago",
  "3 days ago",
  "2 days ago",
  "Yesterday",
  "Today",
];

const backgroundColor = [
  "rgba(255, 11, 32, 0.5)",
  "rgba(255, 152, 63, 0.5)",
  "rgba(255, 220, 120, 0.5)",
  "rgba(0, 201, 192, 0.5)",
  "rgba(52, 176, 239, 0.5)",
  "rgba(197, 53, 255, 0.5)",
  "rgba(201, 203, 207, 0.5)",
];

const borderColor = [
  "rgb(255, 99, 132)",
  "rgb(255, 159, 64)",
  "rgb(255, 205, 86)",
  "rgb(75, 192, 192)",
  "rgb(54, 162, 235)",
  "rgb(153, 102, 255)",
  "rgb(201, 203, 207)",
];

interface IProfileBarProps{
  data: IStatistics[] | undefined
}
ChartJS.register(CategoryScale, LinearScale, BarElement, Tooltip);

const ProfileBar: React.FC<IProfileBarProps> = ({ data }) => {

  const options = {
    responsive: true,
    scales: {
      x: {
        ticks: {
          color: "black",
          font: {
            size: 14,
          },
        },
      },
      y: {
        ticks: {
          font: {
            size: 14,
          },
        },
      },
    },
  };

  return (
    <Bar
      data={{
        labels,
        datasets: [
          {
            data: data?.map((d) => d.count),
            backgroundColor,
            borderColor,
            borderWidth: 2,
          },
        ],
      }}
      options={options}
    />
  );
};

export default ProfileBar;
