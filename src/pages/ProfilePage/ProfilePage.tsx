import { useQuery } from "@apollo/client";
import { GET_MESSAGE_STATISTICS } from "../../common/apolloClient/gql/query/getMessageStatistics";
import Avatar from "../../components/Avatar/Avatar";
import useAuthContext from "../../hooks/useAuthContext";
import { getTokenExpirationDate } from "../../utils/getTokenExpirationDate";
import styles from "./ProfilePage.module.scss";
import Button from "../../components/Button/Button";
import { IGetStatistics } from "../../models/message";
import PreLoader from "../../components/PreLoader/PreLoader";
import ProfileBar from "./ProfileBar";

const ProfilePage = () => {
  const { logout } = useAuthContext();
  const { user } = useAuthContext();
  const { data, loading } = useQuery<IGetStatistics>(GET_MESSAGE_STATISTICS);

  if (loading) return <PreLoader />;

  return (
    <div className={styles.wrapper}>
      <div className={styles.content}>
        <p className={styles.title}>Profile</p>
        <div className={styles.baseInfo}>
          <Avatar isLarge avatarUrl={user?.avatar} />
          <div className={styles.text}>
            <p className={styles.login}>{user?.login}</p>
            <p className={styles.email}>{user?.email}</p>
            <p className={styles.tokenExpired}>Token expires at:</p>
            <p className={styles.tokenExpiredDate}>{getTokenExpirationDate()}</p>
          </div>
        </div>
        <div className={styles.statistic}>
          <h3>Your message statistic</h3>
          <div className={styles.bar}>
            <ProfileBar data={data?.statistics} />
          </div>
        </div>
        <div className={styles.btn}>
          <Button bgType="primary" cb={logout} disabled={loading} >
            Logout
          </Button>
        </div>
      </div>
    </div>
  );
};

export default ProfilePage;
