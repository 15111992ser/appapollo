import React, { useState } from "react";
import ChatContent from "./ChatContent/ChatContent";
import ChatList from "./ChatList/ChatList";
import styles from "./ChatPage.module.scss";



const ChatPage: React.FC = () => {
  const [selectedConv, setSelectedConv] = useState<string>("0");

  return (
    <div className={styles.wrapper}>
      <ChatList
        selectedConv={selectedConv}
        setSelectedConv={setSelectedConv}
      />
      <ChatContent selectedConv={selectedConv} />
    </div>
  );
};

export default ChatPage;
