import AddRoom from "../../../components/AddRoom/AddRoom";
import styles from "./ChatList.module.scss";
import cn from "classnames";
import { useEffect, useState } from "react";
import { useLazyQuery, useSubscription } from "@apollo/client";
import { GET_ALL_CONVERSATIONS } from "../../../common/apolloClient/gql/query/getAllConversations";
import { IConversation } from "../../../models/conversations";
import { CONVERSATION_ADDED } from "../../../common/apolloClient/gql/subscription/conversationAdded";
import PreLoader from "../../../components/PreLoader/PreLoader";
import useMobileContext from "../../../hooks/useMobileContext";

interface IDataMessage {
  getAllConversations: Array<IConversation>;
}

interface IChatListProps {
  selectedConv: string;
  setSelectedConv: React.Dispatch<React.SetStateAction<string>>;
}

interface IDataSubscriptionMessage {
  conversationAdded: Array<IConversation>;
}

const defaultRoom = [{ name: "GENERAL", id: "0" }];

const ChatList: React.FC<IChatListProps> = ({ selectedConv, setSelectedConv }) => {
  const {isVisibleList, setIsVisibleList} = useMobileContext()
  const [conversations, setConversations] = useState<Array<IConversation>>(defaultRoom);
  const [getAllConversations, { loading }] = useLazyQuery<IDataMessage>(GET_ALL_CONVERSATIONS);
  const { data } = useSubscription<IDataSubscriptionMessage>(CONVERSATION_ADDED);

  useEffect(() => {
    const getConversations = async () => {
      const res = await getAllConversations();
      if (res.data) {
        setConversations(conversations.concat(res.data.getAllConversations));
      }
    };
    getConversations();
  }, []);

  useEffect(() => {
    if (conversations && data?.conversationAdded) {
      setConversations(defaultRoom.concat(data.conversationAdded.reverse()));
    }
  }, [data]);

  return (
    <div className={cn(styles.chatList, isVisibleList && styles.open)}>
      <AddRoom />
      {loading ? (
        <PreLoader />
      ) : (
        <div>
          {conversations.map((conv) => (
            <div
              className={styles.itemChat}
              key={conv.id}
              onClick={() => {
                setIsVisibleList(false)
                setSelectedConv(conv.id);
              }}
            >
              <div
                className={cn(styles.selectedChat, selectedConv === conv.id ? styles.active : "")}
              ></div>
              <p className={styles.itemChatName}>{conv.name}</p>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default ChatList;
