import Message from "../../../components/Message/Message";
import styles from "./ChatContent.module.scss";
import { useLazyQuery, useQuery, useSubscription } from "@apollo/client";
import { useEffect, useState } from "react";
import { IMessage } from "../../../models/message";
import useAuthContext from "../../../hooks/useAuthContext";
import { MESSAGE_ADDED } from "../../../common/apolloClient/gql/subscription/messageAdded";
import { GET_ALL_MESSAGES } from "../../../common/apolloClient/gql/query/getAllMessages";
import ChatContentForm from "./ChatContentForm/ChatContentForm";
import PreLoader from "../../../components/PreLoader/PreLoader";
import ErrorComponent from "../../../components/Error/ErrorComponenet";

interface IDataMessage {
  getAllMessages: Array<IMessage>;
}

interface IDataSubscriptionMessage {
  messageAdded: Array<IMessage>;
}

interface IChatContentProps {
  selectedConv: string;
}

const dateSubscription = new Date();
dateSubscription.setFullYear(dateSubscription.getFullYear() + 1);

const ChatContent: React.FC<IChatContentProps> = ({ selectedConv }) => {
  const { user } = useAuthContext();
  const [messages, setMessages] = useState<Array<IMessage> | null>(null);
  const [getAllMessage, { loading, error }] = useLazyQuery<IDataMessage>(GET_ALL_MESSAGES, {
    fetchPolicy: "cache-and-network",
  });
  const { data, loading : loadingSubscription } = useSubscription<IDataSubscriptionMessage>(MESSAGE_ADDED, {
    variables: { date: dateSubscription },
  });

  useEffect(() => {
    const getMessage = async () => {
      setMessages(null);
      const res = await getAllMessage({ variables: { convId: Number(selectedConv) } });
      if (res.data) {
        setMessages(res.data.getAllMessages);
      }
    };
    getMessage();
  }, [getAllMessage, selectedConv]);

  useEffect(() => {
    if (messages && data?.messageAdded.length) {
      const result = [];
      for (let el of data?.messageAdded) {
        if (el.convId === +selectedConv) {
          result.push(el);
        }
      }
      setMessages(messages.concat(result));
    }
  }, [data]);

  return (
    <div className={styles.chatContent}>
      {error && <ErrorComponent err={error.message} />}
      <div className={styles.messagesBloc}>
        {!messages?.length && !loading && loadingSubscription && messages !== null && <PreLoader text="Chat is empty" />}
        {loading ? (
          <PreLoader />
        ) : (
          <div>
            {messages && user &&
              messages.map((m) => (
                <Message
                  key={m.id}
                  avatar={m.user.avatar}
                  messageText={m.description}
                  date={m.date}
                  isIncomingMessages={m.userId !== Number(user?.id)}
                />
              ))}
          </div>
        )}
      </div>
      <ChatContentForm selectedConv={selectedConv} />
    </div>
  );
};

export default ChatContent;
