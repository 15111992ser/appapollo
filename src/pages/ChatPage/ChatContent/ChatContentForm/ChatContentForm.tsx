import React from "react";
import Button from "../../../../components/Button/Button";
import styles from "./ChatContentForm.module.scss";
import { useForm, SubmitHandler } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useLazyQuery, useMutation, useSubscription } from "@apollo/client";
import { IMessage } from "../../../../models/message";
import { CREATE_MESSAGE } from "../../../../common/apolloClient/gql/mutation/createMessage";
import ErrorComponent from "../../../../components/Error/ErrorComponenet";
import { TYPING_USER } from "../../../../common/apolloClient/gql/query/typingUer";
import { IDataUserIsTyping, IDataUserTyping } from "../../../../models/typing";
import { IS_TYPING } from "../../../../common/apolloClient/gql/subscription/isTypinng";
import useAuthContext from "../../../../hooks/useAuthContext";

interface IUseFormMessage {
  message: string;
}

interface IChatContentFormProps {
  selectedConv: string;
}

const schema = yup.object().shape({
  message: yup.string().trim().required(),
});

const ChatContentForm: React.FC<IChatContentFormProps> = ({ selectedConv }) => {
  const { user } = useAuthContext();
  const idAuthUser = user?.id;
  const [createMessage, { loading, error }] = useMutation<IMessage>(CREATE_MESSAGE);
  const { register, handleSubmit, reset, setValue, getValues } = useForm<IUseFormMessage>({
    resolver: yupResolver(schema),
  });
  const [typingUser] = useLazyQuery<IDataUserTyping>(TYPING_USER, {
    fetchPolicy: "cache-and-network",
  });
  const { data: isTypingData } = useSubscription<IDataUserIsTyping>(IS_TYPING, {
    variables: { convId: +selectedConv },
  });

  const loginsTypingUser: Array<string | undefined> = [];

  if (isTypingData?.isTyping) {
    for (const el of isTypingData?.isTyping) {
      if (el.user.id !== idAuthUser) {
        loginsTypingUser.push(el.user.login);
      }
    }
  }

  const onTypingUser = async () => {
    await typingUser({
      variables: {
        convId: +selectedConv,
      },
    });
  };

  const submitForm: SubmitHandler<IUseFormMessage> = async (data) => {
    await createMessage({
      variables: { convId: Number(selectedConv), description: data.message },
    });
    reset();
  };

  const handlerKeyDown: React.KeyboardEventHandler<HTMLFormElement> = (e) => {
    onTypingUser();
    if (e.key === "Enter" && !e.altKey) {
      e.preventDefault();
      handleSubmit(submitForm)();
    }
    if (e.key === "Enter" && e.altKey) {
      setValue("message", getValues("message") + "\n");
    }
  };

  return (
    <>
      <div className={styles.isTypinContainer}>
        {loginsTypingUser.length ? (
          <div className={styles.isTypinBlock}>
            {loginsTypingUser.join(", ")} <span>is typing ...</span>
          </div>
        ) : (
          ""
        )}
      </div>
      {error && <ErrorComponent err={error?.message} />}
      <form
        className={styles.formBlock}
        onSubmit={handleSubmit(submitForm)}
        onKeyDown={handlerKeyDown}
      >
        <textarea className={styles.chatTextarea} {...register("message")}></textarea>
        <Button type="submit" bgType="primary" disabled={loading}>
          <img src="/img/sendIcon.svg" alt="send" />
        </Button>
      </form>
    </>
  );
};

export default ChatContentForm;
