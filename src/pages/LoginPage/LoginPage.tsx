import React from "react";
import styles from "./LoginPage.module.scss";
import Input from "../../components/Input/Input";
import PageLayout from "../../components/PageLayout/PageLayout";
import Button from "../../components/Button/Button";
import ErrorComponent from "../../components/Error/ErrorComponenet";
import { useLazyQuery } from "@apollo/client";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import useAuthContext from "../../hooks/useAuthContext";
import { ISignIn } from "../../models/auth";
import { LOGIN_USER } from "../../common/apolloClient/gql/query/loginUser";

interface IUseForm {
  email: string;
  password: string;
}

interface LocationState {
  from: {
    pathname: string;
  };
}

const schema = yup.object().shape({
  email: yup.string().email().required(),
  password: yup.string().min(6).max(12).required(),
});

const LoginPage: React.FC = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const fromPage = (location.state as LocationState)?.from.pathname || "/";
  const { login } = useAuthContext();
  const [getLoginUser, { error, loading }] = useLazyQuery<ISignIn>(LOGIN_USER);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IUseForm>({
    resolver: yupResolver(schema),
  });

  const submitForm = async (data: IUseForm) => {
    const res = await getLoginUser({
      variables: { password: data.password, email: data.email },
    });
    if (res.data) {
      login(res.data?.signIn, () => {
        navigate(fromPage);
      });
    }
  };

  return (
    <PageLayout>
      <div className="wrapperForm">
        <h1 className="titleForm">Welcome</h1>
        {error && <ErrorComponent err={error?.message} />}
        <div className="wrapperFormInner">
          <form className={styles.form} onSubmit={handleSubmit(submitForm)}>
            <Input
              register={register("email")}
              type="text"
              error={errors.email?.message}
              placeholder="Email"
              title="Email"
            />

            <Input
              register={register("password")}
              type="password"
              error={errors.password?.message}
              placeholder="Password"
              title="Password"
            />

            <div className={styles.btnsWrapper}>
              <Link to="/registration">
                <span>Registration</span>
              </Link>
              <Button type="submit" bgType="primary" disabled={loading}>
                Login
              </Button>
            </div>
          </form>
        </div>
      </div>
    </PageLayout>
  );
};

export default LoginPage;
