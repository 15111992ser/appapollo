import { useEffect, useState } from "react";
import { Routes, Route } from "react-router-dom";
import Header from "./components/Header/Header";
import PageLayout from "./components/PageLayout/PageLayout";
import RequireAuth from "./components/RequireAuth/RequireAuth";
import useAuthContext from "./hooks/useAuthContext";
import ChatPage from "./pages/ChatPage/ChatPage";
import LoginPage from "./pages/LoginPage/LoginPage";
import ProfilePage from "./pages/ProfilePage/ProfilePage";
import RegistrationPage from "./pages/RegistrationPage/RegistrationPage";

const App: React.FC = () => {
  const { getUser } = useAuthContext();
  
  useEffect(() => {
    getUser();
  }, []);

  return (
    <>
      <Header />
      <PageLayout>
        <Routes>
          <Route
            path="/"
            element={
              <RequireAuth>
                <ChatPage  />
              </RequireAuth>
            }
          />
          <Route
            path="/profile"
            element={
              <RequireAuth>
                  <ProfilePage />
              </RequireAuth>
            }
          />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/registration" element={<RegistrationPage />} />
        </Routes>
      </PageLayout>
    </>
  );
};

export default App;
