export const formatDate = (d: any) => {
  const date = d.getDate().toString().padStart(2, "0");
  const month = (d.getMonth() + 1).toString().padStart(2, "0");
  const year = d.getFullYear();
  const h = d.getHours().toString().padStart(2, "0");
  const m = d.getMinutes().toString().padStart(2, "0");

  return `${date}.${month}.${year} ${h}:${m}`;
};

export const getFormatDateMessages = (payload: string) => {
  const d = new Date(payload);
  const currentDayDate = new Date().getDate();
  const currentMonth = new Date().getMonth();
  const messageDayDate = d.getDate();
  const messageMonthDate = d.getMonth();

  if (messageDayDate < currentDayDate || messageMonthDate < currentMonth) {
    
   return formatDate(d);
  } else {
    const h = d.getHours().toString().padStart(2, "0");
    const m = d.getMinutes().toString().padStart(2, "0");

    return `${h}:${m}`;
  }
};