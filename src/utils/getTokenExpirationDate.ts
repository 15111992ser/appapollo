import { formatDate } from "./formatTime";

export const getTokenExpirationDate = () => {
    const JWT = localStorage.getItem("access-token");
    if (JWT) {
      const jwtPayload = JSON.parse(window.atob(JWT.split(".")[1]));
      const dateJwt = jwtPayload.exp * 1000;
      const d = new Date(dateJwt);
  
     return formatDate(d);
    }
  };