import { IUserData } from "./user";

export interface IRegistration {
  registration: IUserData;
}

export interface ISignIn {
  signIn: IUserData;
}