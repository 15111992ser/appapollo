import { IUser } from "./user";

export interface IMessage {
  id: string;
  description: string;
  userId: number;
  convId: number;
  date: string;
  user: {
    avatar: string;
  };
}

export interface IStatistics {
  date: Date;
  count: number;
}
export interface IGetStatistics {
  statistics: IStatistics[];
}
