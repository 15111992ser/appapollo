import { IUser } from "./user";

interface IDataTyping {
  user: IUser;
  convId: number;
  date: Date;
}

export interface IDataUserTyping {
  typingUser: Array<IDataTyping>;
}

export interface IDataUserIsTyping {
  isTyping: Array<IDataTyping>
}
