export interface IConversation {
  id: string;
  createdBy?: string;
  name: string;
  date?: string;
}
