import React from "react";
import ReactDOM from "react-dom/client";
import "normalize.css";
import "./common/styles/index.scss";
import App from "./App";
import { ApolloProvider } from "@apollo/client";
import client from "./common/apolloClient/apolloClient";
import { BrowserRouter } from "react-router-dom";
import AuthContextProvider from "./context/authContext";
import MobileContextProvider from "./context/mobileContext";

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement);
root.render(
  <ApolloProvider client={client}>
    <BrowserRouter>
      <React.StrictMode>
        <MobileContextProvider>
          <AuthContextProvider>
            <App />
          </AuthContextProvider>
        </MobileContextProvider>
      </React.StrictMode>
    </BrowserRouter>
  </ApolloProvider>
);
