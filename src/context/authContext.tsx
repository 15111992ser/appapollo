import { useLazyQuery } from "@apollo/client";
import React, { useReducer } from "react";
import { useNavigate } from "react-router-dom";
import { GET_ME } from "../common/apolloClient/gql/query/getMe";
import { IUser } from "../models/user";
import { IUserData } from "../models/user";

interface IMe {
  me: {
    user: IUser;
  };
}

interface IContext {
  isAuth: boolean;
  user: IUser | null;
  login: (data: IUserData, cb: VoidFunction) => void;
  logout: () => void;
  getUser: (from?: string) => void;
}

interface IAuthContextProvider {
  children: JSX.Element;
}

export const AuthContext = React.createContext<IContext>({
  isAuth: false,
  user: null,
  login: (userData: IUserData) => {},
  logout: () => {},
  getUser: (from?: string) => {},
});

const initialState = { user: null, isAuth: false };

const authReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case "LOGIN":
      return { ...state, user: action.payload, isAuth: true };
    case "LOGOUT":
      return { ...state, user: null, isAuth: false };
    default:
      return state;
  }
};

const AuthContextProvider: React.FC<IAuthContextProvider> = ({ children }) => {
  const navigate = useNavigate();
  const [state, dispatch] = useReducer(authReducer, initialState);
  const [getMe] = useLazyQuery<IMe>(GET_ME);

  const getUser = async (from?: string) => {
    try {
      if (localStorage.getItem("access-token") && !state.user) {
        const res = await getMe();
        if (res.data) {
          login(res.data?.me, () => {
            if (from) navigate(from);
          });
        }
      }
    } catch {
      logout();
    }
  };

  const login = (userData: IUserData, cb: () => void) => {
    dispatch({
      type: "LOGIN",
      payload: userData.user,
    });

    if (userData?.token) {
      localStorage.setItem("access-token", userData.token);
      cb();
    }
  };

  const logout = () => {
    dispatch({
      type: "LOGOUT",
    });
    localStorage.removeItem("access-token");
  };

  return (
    <AuthContext.Provider
      value={{ user: state.user, isAuth: state.isAuth, login, logout, getUser }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContextProvider;
