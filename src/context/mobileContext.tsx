import React, { useState } from "react";

interface IMobileContext {
  isVisibleList: boolean;
  setIsVisibleList: React.Dispatch<React.SetStateAction<boolean>>;
}


export const MobileContext = React.createContext<IMobileContext>({
  isVisibleList: false,
  setIsVisibleList: ()=> {}
});

interface IMobMobileContextProviderProps {
  children: JSX.Element;
}

const MobileContextProvider: React.FC<IMobMobileContextProviderProps> = ({ children}) => {
  const [isVisibleList, setIsVisibleList] = useState<boolean>(false);

  return (
    <MobileContext.Provider
      value={{ isVisibleList, setIsVisibleList}}
    >
      {children}
    </MobileContext.Provider>
  );
};

export default MobileContextProvider;
