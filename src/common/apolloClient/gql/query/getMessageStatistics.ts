import { gql } from "@apollo/client";

export const GET_MESSAGE_STATISTICS = gql`
query {
  statistics: getMessageStatistics {
      count
      date
    }
  }
`;
