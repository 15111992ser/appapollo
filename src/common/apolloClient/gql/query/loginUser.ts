import { gql } from "@apollo/client";

export const LOGIN_USER = gql`
  query singin($password: String!, $email: String!) {
    signIn(password: $password, email: $email) {
      token
      user {
        id
        login
        email
        avatar
      }
    }
  }
`;