import { gql } from "@apollo/client";

export const REGISTER_USER = gql`
  mutation registration($login: String!, $email: String!, $password: String!, $avatar: String!) {
    registration(login: $login, email: $email, password: $password, avatar: $avatar) {
      token
      user {
        id
        login
        email
        avatar
      }
    }
  }
`;

export const CREATE_MESSAGE = gql`
  mutation createMessage($convId: Int, $description: String!) {
    createMessage(convId: $convId, description: $description) {
      id
      description
      userId
      convId
      date
      user {
        avatar
      }
    }
  }
`;
