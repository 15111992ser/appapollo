import { gql } from "@apollo/client";

export const CREATE_MESSAGE = gql`
mutation createMessage($convId: Int, $description: String!) {
  createMessage(convId: $convId, description: $description) {
    id
    description
    userId
    convId
    date
    user {
      avatar
    }
  }
}
`;