import { ApolloClient, createHttpLink, InMemoryCache, split } from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import { SubscriptionClient } from "subscriptions-transport-ws";
import { WebSocketLink } from "@apollo/client/link/ws";
import { getMainDefinition } from "@apollo/client/utilities";

const httpLink = createHttpLink({
  uri: process.env.REACT_APP_SERVER_URL,
});

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem("access-token");
  return {
    headers: {
      ...headers,
      "access-token": token || "",
    },
  };
});

const wsLink = new WebSocketLink(
  new SubscriptionClient(process.env.REACT_APP_SERVER_URL_WS || "", {
    reconnect: true,
    lazy: true,
    connectionParams: {
      "access-token": localStorage.getItem("access-token")
    },
  })
);

const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return definition.kind === "OperationDefinition" && definition.operation === "subscription";
  },
  wsLink,
  authLink.concat(httpLink)
);

const client = new ApolloClient({
  link: splitLink,
  cache: new InMemoryCache(),
});

export default client;
